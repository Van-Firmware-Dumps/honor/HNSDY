#!/vendor/bin/sh

pids=$(ps -e | grep -e dsp_send_thread -e dsp_recv_thread | awk '{print $2}')
for pid in $pids; do echo $pid > /dev/cpuctl/tasks;done;
